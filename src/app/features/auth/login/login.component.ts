import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faEyeSlash, faEye } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { LoginResponse } from 'src/app/shared/models/response/login.model';
import { LoginRequest } from 'src/app/shared/models/request/login.model';
import { AuthenticationService } from 'src/app/shared/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  loginForm!: FormGroup;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  passwordVisible = false;
  EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
  PASSWORD_REGEX = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}";

  submitting:boolean=false;

  constructor(
    private _fb: FormBuilder,
    private authService: AuthenticationService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.createForm();
  }

  private createForm(): void {
    this.loginForm = this._fb.group({
      userName: ['admin@mail.com', [Validators.required, Validators.pattern(this.EMAIL_REGEX)]],
      password: ['Admin@123', [Validators.required, Validators.pattern(this.PASSWORD_REGEX)]]
    });
  }

  togglePasswordVisibility() {
    this.passwordVisible = !this.passwordVisible;
  }

  loginUser() {
    this.submitting=true;
    if (this.loginForm.valid) {
      const request: LoginRequest = {
        userName: this.loginForm.get('userName')?.value,
        password: this.loginForm.get('password')?.value
      };
  
      this.authService.login(request)
        .subscribe({
          next: (response: LoginResponse) => {
            this.submitting=false;
            console.log('Login:', response.token);
            if (response && response.token) {
              this.toastr.success('Login successful');
              this.router.navigate(['/dashboard/home']);
            } else {
              this.toastr.error('Invalid response from server');
            }
          },
          error: (error) => {
            this.submitting=false;
            console.error('Login error:', error);
            this.toastr.error('Error during login');
            this.router.navigate(['/login']);
          },
        });
    }
  }
  

    isEmailInvalid(): boolean {
      const emailControl = this.loginForm.get('userName');
      return emailControl!.hasError('pattern') && emailControl!.touched;
    }
  
    isPasswordPatternInvalid(): boolean {
      const passwordControl = this.loginForm.get('password');
      return passwordControl!.hasError('pattern') && passwordControl!.touched;
    }
}

