import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { EmployeeListResponse } from 'src/app/shared/models/response/employee.model';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],

})


export class EmployeeListComponent implements OnInit{
  @ViewChild('menuContainer') menuContainer!: ElementRef;
  modalContent:string='employee';
  sidebar:boolean=false;
  isStarClicked = false;
  baseUrl = 'https://restaurantapi.bssoln.com/images/'
  employeeList: EmployeeListResponse = this.getDefaultEmployeeList();
  isLoading:boolean=false;
  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPage: any  = 10;
  itemsPerPageOptions: number[] = [10, 20, 30, 50];

  isMenuOpen = false;
  showDeleteConfirmation = false;
  employeeId!: string ;
  

  starredEmployees: Set<string | undefined> = new Set<string | undefined>();

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }


  constructor(private restaurentService: RestaurantService, private toastr: ToastrService, private http: HttpClient, private _cdRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getEmployee('first');
    this.fetchEmployeeImages();
  }


  @HostListener('document:click', ['$event'])
  documentClick(event: Event) {
    if (this.isMenuOpen && !this.menuContainer.nativeElement.contains(event.target)) {
      this.toggleMenu();
    }
  }

  handleItemPerPage(size:number){
    this.itemsPerPage=size;
    this.currentPage=1;
    console.log(this.itemsPerPage);
    this.getEmployee('first');
    this.toggleMenu();
  }


  goToPage(action: string) {
    switch (action) {
      case 'first':
        this.currentPage = 1;
        break;
      case 'last':
        this.currentPage = this.totalPages;
        break;
      case 'forward':
        if (this.currentPage < this.totalPages) {
          this.currentPage++;
        }
        break;
      case 'backward':
        if (this.currentPage > 1) {
          this.currentPage--;
        }
        break;
    }
    this.getEmployee(action);
  }
  
  getEmployee(action: string) {
    this.isLoading=true;
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };
  
    this.restaurentService.getEmployeeList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.employeeList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
            console.log(this.employeeList);
            if (action === 'first' || action === 'last') {
              this.currentPage = response.current_page;
            }
          }, 500); 
        },
        error: (error) => {
          console.error(error);
        },
      });
  }
  


  deleteEmployee(employeeId: string ): void {
    console.log(employeeId);
    this.restaurentService.deleteEmployee(employeeId)
    .subscribe({
      next: (response) => {
        this.toastr.success('Deleted Successfully !');
        this.getEmployee('first'); 
        this.showDeleteConfirmation = false;
      },
      error: (error) => {
        this.toastr.error('Something is Wrong !');
      },
    });
  }
  
  deleteEmployeeFirst(){
    this.deleteEmployee(this.employeeId);
  }
  


  fetchEmployeeImages() {
    const apiUrl = 'https://source.unsplash.com/100x100/?person';
    console.log('tri');
  
    const imageRequests = this.employeeList?.data?.map(employee =>
      this.http.get(apiUrl, { responseType: 'text' })
    ) || [];
  
    forkJoin(imageRequests).subscribe(
    
      imageUrls => {
        this.employeeList?.data?.forEach((employee, index) => {
          if (employee && employee.user) {
              console.log("calling");
            employee.user.image = imageUrls[index];
          }
        });
      },
      error => {
        console.error('Error fetching images:', error);
      }
    );
  }



  getDefaultEmployeeList(): EmployeeListResponse {
    return {
      pageNumber: 0,
      current_page: 0,
      per_page: 0,
      pageSize: 0,
      firstPage: '',
      lastPage: '',
      last_page: 0,
      totalPages: 0,
      totalRecords: 0,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: null,
      prev_page_url: null,
      data: [],
    };
  }


  openDeleteConfirmationModal(employeeId: string): void {
    this.employeeId = employeeId;
    this.showDeleteConfirmation = true;
  }

  cancelDelete(): void {
    this.showDeleteConfirmation = false;
  }


  onClickStar(employeeId: string | undefined) {
    if (this.starredEmployees.has(employeeId)) {
      this.starredEmployees.delete(employeeId);
    } else {
      this.starredEmployees.add(employeeId);
    }
  }
  

}

