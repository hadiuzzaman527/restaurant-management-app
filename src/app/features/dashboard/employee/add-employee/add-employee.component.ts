import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NavbarComponent } from 'src/app/shared/components/app-header/app-header.component';
import { AddEmployeeModel } from 'src/app/shared/models/request/add-employee.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})

export class AddEmployeeComponent {
  sidebar:boolean=false;
  newEmployeeForm!: FormGroup;
  selectedImage: string | ArrayBuffer | null = null;
  imageName:string='';

  constructor(
    private _fb: FormBuilder, 
    private datePipe: DatePipe,
    private restaurentService: RestaurantService,
    private toastr: ToastrService,
    private _router:Router
    ) {
    this.createForm();
  }


  handleFileInput(event: any): void {
    const files = event.target.files;

    if (files && files.length > 0) {
      const selectedFile = files[0];
      const fileName = selectedFile.name;
      this.imageName = fileName;

      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target?.result) {
          const base64String = `data:${selectedFile.type};base64,${e.target.result.toString().split(',')[1]}`;

          this.selectedImage = base64String;
          this.newEmployeeForm.patchValue({
            base64: '',
          });
        }
      };
      reader.readAsDataURL(selectedFile);
    }
  }



  removeImage(): void {
    this.selectedImage = null;
    this.newEmployeeForm.patchValue({ image: null });

    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      fileInput.value = '';
    }
  }



  isFieldInvalid(fieldName: string): boolean {
    const control = this.newEmployeeForm.get(fieldName);
    return (
      (control!.hasError('required')) && control!.touched
    );
  }


  private createForm(): void {
    this.newEmployeeForm = this._fb.group({
      designation: ['', [Validators.required]],
      joinDate: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      middleName: [''],
      lastName: ['', [Validators.required]],
      fatherName: ['', [Validators.required]],
      motherName: ['', [Validators.required]],
      spouseName: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      nid: ['', [Validators.required]],
      genderId: ['', [Validators.required]],
      image: [''],
      base64: ['']
    });
  }


  addEmployee() {
    this.newEmployeeForm.markAllAsTouched();
    if (this.newEmployeeForm.valid) {

      this.newEmployeeForm.patchValue({
        joinDate: "2023-12-26T21:21:35.124Z",
        image:'',
        dob: "2023-12-26T21:21:35.124Z",
        genderId: Number(this.newEmployeeForm.value.genderId),
      });

      const request: AddEmployeeModel = this.newEmployeeForm.value;
      console.log("data is ready!", request);
      this.restaurentService.addEmployee(request)
        .subscribe({
          next: (response) => {
            this.toastr.success('Employee Added Successfully !');
            this._router.navigate([`dashboard/employee-list`], { replaceUrl: true});
          },
          error: (error) => {
            this.toastr.error('Something is Wrong !');
            console.error(error);
          },
        });
    }
  }



}
