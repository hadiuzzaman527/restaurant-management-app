import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { TableDialogueComponent } from 'src/app/shared/components/table-dialogue/table-dialogue.component';
import { NonAssignEmployee } from 'src/app/shared/models/request/non-assign-employee.model';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { EmployeeListResponse } from 'src/app/shared/models/response/employee.model';
import { TableListResponse } from 'src/app/shared/models/response/table.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { SharedDataService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html'
})
export class TableListComponent implements OnInit{
  @ViewChild('menuContainer') menuContainer!: ElementRef;
  modalContent:string='';

  tableList: TableListResponse = this.getDefaultTableList();
  sidebar:boolean=false;
  
  isLoading:boolean=false;
  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPageOptions: number[] = [10, 20, 30, 50];
  isMenuOpen = false;
  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }
  itemsPerPage: any  = 10;
  showDeleteConfirmation = false;
  tableId!: number;
  employeeId!:string;
  numberOfSeats:number=0;
  constructor(
    private restaurentService: RestaurantService, 
    private sharedService:SharedDataService,
    private toastr: ToastrService,
    public dialog: MatDialog
    ) 
    { }

  ngOnInit(): void {
    this.getTableList('first');
    this.sharedService.refreshTableList$.subscribe(() => {
      this.getTableList('first');  
    });
  }


  @HostListener('document:click', ['$event'])
  documentClick(event: Event) {
    if (this.isMenuOpen && !this.menuContainer.nativeElement.contains(event.target)) {
      this.toggleMenu();
    }
  }



  openDialog(tableId:number,numberOfSeats:number): void {
    const dialogRef = this.dialog.open(TableDialogueComponent, {
      width: 'auto',
      height: 'auto',
      data: {
        tableId: tableId,
        numberOfSeats: numberOfSeats
      },
      
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  handleItemPerPage(size:number){
    this.itemsPerPage=size;
    this.currentPage=1;
    console.log(this.itemsPerPage);
    this. getTableList('first');
    this.toggleMenu();
  }


  goToPage(action: string) {
    switch (action) {
      case 'first':
        this.currentPage = 1;
        break;
      case 'last':
        this.currentPage = this.totalPages;
        break;
      case 'forward':
        if (this.currentPage < this.totalPages) {
          this.currentPage++;
        }
        break;
      case 'backward':
        if (this.currentPage > 1) {
          this.currentPage--;
        }
        break;
    }
    this. getTableList(action);
  }


  getTableList(action:string){
    this.isLoading = true;
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };
  
    this.restaurentService.getTableList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.tableList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
            console.log(this.tableList);
            if (action === 'first' || action === 'last') {
              this.currentPage = response.current_page;
            }
          }, 500); 
        },
        error: (error) => {
          console.error(error);
        },
      });
  }

  deleteTable(tableId: number): void {
    console.log(tableId);
    this.restaurentService.deleteTable(tableId)
    .subscribe({
      next: (response) => {
        this.toastr.success('Deleted Successfully !');
        this.getTableList('first'); 
        this.showDeleteConfirmation = false;
      },
      error: (error) => {
        this.toastr.error('Something is Wrong !');
      },
    });
  }
  

  deleteTableFirst(){
    console.log(this.modalContent);
    if( this.modalContent==='table'){
      this.deleteTable(this.tableId);
    }
    else if(this.modalContent==='employee')
  {
      this.deleteEmployee(this.employeeId);
    

  }
  
  }
  getDefaultTableList(): TableListResponse {
    return  {
      pageNumber: 0,
      current_page: 1,
      per_page: 0,
      pageSize: 0,
      firstPage: '...',
      lastPage: '...',
      last_page: 0,
      totalPages: 0,
      totalRecords: 0,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: null,
      prev_page_url: null,
      data: [ ],
    };

  }

  deleteEmployee(employeeId: string): void {
    console.log(employeeId);
    this.restaurentService.deleteEmployee(employeeId)
    .subscribe({
      next: (response) => {
        this.toastr.success('Deleted Successfully !');
        this.getTableList('first'); 
        this.showDeleteConfirmation = false;
      },
      error: (error) => {
        this.toastr.error('Something is Wrong !');
      },
    });
  }


  openDeleteConfirmationModal(id: number | string): void {
    if (typeof id === 'number') {
      this.tableId = id;
      this.modalContent='table'
    } else if (typeof id === 'string') {
      this.employeeId = id;
      this.modalContent='employee';
    }
  
    this.showDeleteConfirmation = true;
  }
  

  cancelDelete(): void {
    this.showDeleteConfirmation = false;
  }


  
}
