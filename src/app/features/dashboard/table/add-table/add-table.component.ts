import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddTableRequest } from 'src/app/shared/models/request/add-table.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-add-table',
  templateUrl: './add-table.component.html'
})
export class AddTableComponent {
  tableForm!: FormGroup;
  selectedImage: string | ArrayBuffer | null = null;
  imageName: string = '';
  sidebar:boolean=false;
  discountId: number = 0;
  discountedPrice: number = 0;
  statusSortingValue = [
    { label: 0, id: 0 },
    { label: 2, id: 1 },
    { label: 4, id: 2 },
    { label: 6, id: 3 },
    { label: 8, id: 4 },
    { label: 10, id: 5 }
  ]

  constructor(
    private _fb: FormBuilder,
    private restaurentService: RestaurantService,
    private toastr: ToastrService,
    private _router: Router
  ) {
    this.createForm();
  }

  handleFileInput(event: any): void {
    const files = event.target.files;

    if (files && files.length > 0) {
      const selectedFile = files[0];
      const fileName = selectedFile.name;
      this.imageName = fileName;

      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target?.result) {
          const base64String = `data:${selectedFile.type};base64,${e.target.result.toString().split(',')[1]}`;

          this.selectedImage = base64String;
          this.tableForm.patchValue({
            base64: '',
          });
        }
      };
      reader.readAsDataURL(selectedFile);
    }
  }

  removeImage(): void {
    this.selectedImage = null;
    this.tableForm.patchValue({ image: null });
    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      fileInput.value = '';
    }
  }


  selectedNumberOfSeats(numberOfSeats:number){
    this.tableForm.patchValue({ numberOfSeats: numberOfSeats });
  }


  addTable(){
    this.tableForm.markAllAsTouched();
    
    if (this.tableForm.value) {
      const request: AddTableRequest = this.tableForm.value;
      this.restaurentService.addTable(request)
        .subscribe({
          next: (response) => {
            this.toastr.success('Table Added Successfully !');
            this._router.navigate([`dashboard/table-list`], { replaceUrl: true });
          },
          error: (error) => {
            this.toastr.error('Something is Wrong !');
            console.error(error);
          },
        });
    }
  }


  private createForm(): void {
    this.tableForm = this._fb.group({
      numberOfSets: ['', [Validators.required]],
      tableNumber: ['', [Validators.required]],
      image: [''],
      base64: ['']
    });
  }




}

