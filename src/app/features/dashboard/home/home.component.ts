import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { FoodItem, FoodListResponse } from 'src/app/shared/models/response/food.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { SharedDataService } from 'src/app/shared/services/shared.service';
interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  // categories$: Observable<Array<string>>;

  // constructor(private dataService: SharedDataService) {
  //   this.categories$ = dataService.categories$;
  // }

  // onScrollingFinished() {
  //   console.log('load more');
  //   this.dataService.loadMore();
  // }
  
  foodItem: FoodItem[]=[];
  parentLoader:boolean=false;
  isLoading: boolean = false;
  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPage: any = 5;
  baseUrl = 'https://restaurantapi.bssoln.com/images/'

  constructor(
    private restaurentService: RestaurantService,
    private sharedDataService: SharedDataService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getFoodList();
  }


  onScrollingFinished() {
    console.log('load more');
    this.currentPage++;
    console.log(this.currentPage <= this.totalPages);
    
    if (this.currentPage <= this.totalPages) {
      this.getFoodList();
    }

  }


  getFoodList() {
    if(this.currentPage===1){
      this.parentLoader=true;
    }
    if(this.currentPage>1){
      this.isLoading = true;
    }
   
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };

    this.restaurentService.getFoodList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.foodItem = [...this.foodItem, ...response.data];
            this.parentLoader=false;
            this.isLoading = false;

            this.totalPages = response.totalPages;

          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }


getDefaultFoodList(): FoodListResponse {
  return {
    pageNumber: 1,
    current_page: 1,
    per_page: 0,
    pageSize: 0,
    firstPage: '',
    lastPage: '',
    last_page: 1,
    totalPages: 1,
    totalRecords: 4,
    total: 0,
    from: 0,
    to: 0,
    next_page_url: null,
    prev_page_url: null,
    data: []
  };
}

}
