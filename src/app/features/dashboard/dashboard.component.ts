import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faHouseUser,faUser,faTableList,faBurger,faUtensils,faFileInvoice,faRightFromBracket} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  faHouseUser = faHouseUser;
  faUser = faUser;
  faTableList = faTableList;
  faBurger = faBurger;
  faUtensils = faUtensils;
  faFileInvoice = faFileInvoice;
  faRightFromBracket = faRightFromBracket;
  menuItems = [
    { label: 'Home', routerLink: '/dashboard/home', icon: this.faHouseUser },
    { label: 'All Employee List', routerLink: '/dashboard/employee-list', icon: this.faUser },
    { label: 'All Table List', routerLink: '/dashboard/table-list', icon: this.faTableList },
    { label: 'All Food List', routerLink: '/dashboard/food-list', icon: this.faBurger },
    { label: 'Order Food', routerLink: '/dashboard/order', icon: this.faUtensils },
    { label: 'Order List', routerLink: '/dashboard/order-list', icon: this.faFileInvoice },
  ];
}
