import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { FoodListResponse } from 'src/app/shared/models/response/food.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { SharedDataService } from 'src/app/shared/services/shared.service';


@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.css']
})

export class FoodListComponent {
  @ViewChild('menuContainer') menuContainer!: ElementRef;
  foodList: FoodListResponse = this.getDefaultFoodList();
  isLoading: boolean = false;
  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPageOptions: number[] = [10, 20, 30, 50];
  itemsPerPage: any = 10;

  isMenuOpen = false;
  sidebar: boolean = false;
  baseUrl = 'https://restaurantapi.bssoln.com/images/'
  modalContent: string = 'food';
  showDeleteConfirmation = false;
  foodIdForDelete!: number;


  newFoodList!:any[];
  newCurrentPage: any = 1;
  newTotalPages: any = 1;

  constructor(
    private restaurentService: RestaurantService,
    private sharedDataService: SharedDataService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getFoodList('first');
    }

  @HostListener('document:click', ['$event'])
  documentClick(event: Event) {
    if (this.isMenuOpen && !this.menuContainer.nativeElement.contains(event.target)) {
      this.toggleMenu();
    }
  }


  handleItemPerPage(size: number) {
    this.itemsPerPage = size;
    this.currentPage = 1;
    console.log(this.itemsPerPage);
    this.getFoodList('first');
    this.toggleMenu();
  }


  goToPage(action: string) {
    switch (action) {
      case 'first':
        this.currentPage = 1;
        break;
      case 'last':
        this.currentPage = this.totalPages;
        break;
      case 'forward':
        if (this.currentPage < this.totalPages) {
          this.currentPage++;
        }
        break;
      case 'backward':
        if (this.currentPage > 1) {
          this.currentPage--;
        }
        break;
    }
    this.getFoodList(action);
  }


  deleteFoodFirst() {
    this.deleteFood(this.foodIdForDelete);
  }

  getFoodList(action: string) {
    const cachedData = this.sharedDataService.getSelectedFoodListForPage(this.currentPage);

    if (cachedData) {
      this.foodList = cachedData;
      this.isLoading = false;
  
      if (cachedData.totalPages) {
          this.totalPages = cachedData.totalPages;
          this.currentPage = cachedData.current_page;
      }
  } else {
        this.isLoading = true;
        const paginationModel: PaginationModel = {
            page: this.currentPage,
            size: this.itemsPerPage,
        };

        this.restaurentService.getFoodList(paginationModel)
            .subscribe({
                next: (response) => {
                    setTimeout(() => {
                        this.foodList = response;
                        this.isLoading = false;
                        this.totalPages = response.totalPages;

                        this.sharedDataService.setSelectedFoodListWithPage(response);

                        if (action === 'first' || action === 'last') {
                            this.currentPage = response.current_page;
                        }
                    }, 500);
                },
                error: (error) => {
                    console.error(error);
                },
            });
    }
}

  getDefaultFoodList(): FoodListResponse {
    return {
      pageNumber: 1,
      current_page: 1,
      per_page: 0,
      pageSize: 0,
      firstPage: '',
      lastPage: '',
      last_page: 1,
      totalPages: 1,
      totalRecords: 4,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: null,
      prev_page_url: null,
      data: []
    };
  }



  deleteFood(foodIdForDelete: number): void {
    console.log(foodIdForDelete);
    this.restaurentService.deleteFood(foodIdForDelete)
      .subscribe({
        next: (response) => {
          this.toastr.success('Deleted Successfully !');
          this.getFoodList('first');
          this.showDeleteConfirmation = false;
        },
        error: (error) => {
          this.toastr.error('Something is Wrong !');
        },
      });
  }



  openDeleteConfirmationModal(foodId: number): void {
    this.foodIdForDelete = foodId;
    this.showDeleteConfirmation = true;
  }

  cancelDelete(): void {
    this.showDeleteConfirmation = false;
  }


  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

}
