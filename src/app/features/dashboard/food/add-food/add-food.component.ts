import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FoodRequest } from 'src/app/shared/models/request/add-food.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.component.html',
  styleUrls: ['./add-food.component.css']
})
export class AddFoodComponent implements OnInit {
  foodForm!: FormGroup;
  selectedImage: string | ArrayBuffer | null = null;
  imageName: string = '';
  sidebar:boolean=false;
  discountId: number = 0;
  discountedPrice: number = 0;
  statusSortingValue = [
    { label: 'None', id: 0 },
    { label: 'Flat', id: 1 },
    { label: 'Percentage', id: 2 }
  ]

  constructor(
    private _fb: FormBuilder,
    private restaurentService: RestaurantService,
    private toastr: ToastrService,
    private _router: Router
  ) {
    this.createForm();
  }



  ngOnInit(): void {
    const discountControl = this.foodForm.get('discount');
    if (discountControl) {
      discountControl.valueChanges.subscribe((discountValue) => {
        this.updateDiscountedPrice();
      });
    }
  }




  handleFileInput(event: any): void {
    const files = event.target.files;

    if (files && files.length > 0) {
      const selectedFile = files[0];
      const fileName = selectedFile.name;
      this.imageName = fileName;

      const reader = new FileReader();
      reader.onload = (e) => {
        if (e.target?.result) {
          const base64String = `data:${selectedFile.type};base64,${e.target.result.toString().split(',')[1]}`;

          this.selectedImage = base64String;
          this.foodForm.patchValue({
            base64: '',
          });
        }
      };
      reader.readAsDataURL(selectedFile);
    }
  }



  removeImage(): void {
    this.selectedImage = null;
    this.foodForm.patchValue({ image: null });

    const fileInput = document.getElementById('fileInput') as HTMLInputElement;
    if (fileInput) {
      fileInput.value = '';
    }
  }



  isFieldInvalid(fieldName: string): boolean {
    const control = this.foodForm.get(fieldName);
    return (
      (control!.hasError('required')) && control!.touched
    );
  }


  private createForm(): void {
    this.foodForm = this._fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      price: ['', [Validators.required]],
      discountType: [''],
      discount: ['', [Validators.required]],
      discountPrice: ['', [Validators.required]],
      image: [''],
      base64: ['']
    });
  }


  selectedDiscountType(discountId: number) {
    this.discountId = discountId;

    this.foodForm.patchValue({
      discount: '',
      discountPrice: ''
    });
  }


  private updateDiscountedPrice() {
    const price = this.foodForm.get('price')?.value || 0;
    const discount = this.foodForm.get('discount')?.value || 0;

    if (this.discountId === 1) {
      this.discountedPrice = price - discount;
      this.foodForm.patchValue({
        discountPrice: this.discountedPrice,
        discountType:Number(this.discountId)
      });
    } else if (this.discountId === 2) {
      const discountedPrice = price - (price * discount) / 100;
      this.discountedPrice = discountedPrice;
      this.foodForm.patchValue({
        discountPrice: discountedPrice,
        discountType:Number(this.discountId)
      });
    }
  }



  addFood() {
    this.foodForm.markAllAsTouched();
    this.updateDiscountedPrice();

    if (this.foodForm.value) {
      const request: FoodRequest = this.foodForm.value;
      console.log("data is ready!", request);
      this.restaurentService.addFood(request)
        .subscribe({
          next: (response) => {
            this.toastr.success('Food Added Successfully !');
            this._router.navigate([`dashboard/food-list`], { replaceUrl: true });
          },
          error: (error) => {
            this.toastr.error('Something is Wrong !');
            console.error(error);
          },
        });
    }
  }




}
