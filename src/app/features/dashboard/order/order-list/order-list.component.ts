import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { SortPaginationModel } from 'src/app/shared/models/request/sortPagination.model';
import { UpdateStatusRequest } from 'src/app/shared/models/request/update-status.model';
import { OrderListModel } from 'src/app/shared/models/response/order-list.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html'
})
export class OrderListComponent {
  NonAssignEmployee: any[] = [];

  orderList: OrderListModel = this.getDefaultOrderList();
  isLoading: boolean = false;
  currentPage: any = 1;
  totalPages: any = 10;
  itemsPerPage: any = 10;
  sidebar:boolean=false;
  toogleFoodStatus: boolean = false;
  toggleValue: string = '';

  modalContent:string='order';
  showDeleteConfirmation = false;
  orderId!: string ;

  statusValue = [
    { label: 'PreparedToServe', id: 3 },
    { label: 'Pending', id: 0 },
    { label: 'Confirmed', id: 1 },
    { label: 'Preparing', id: 2 },
    { label: 'Served', id: 4 },
    { label: 'Paid', id: 5 }
  ]

  statusSortingValue = [
    { label: 'All', id: 100 },
    { label: 'PreparedToServe', id: 3 },
    { label: 'Pending', id: 0 },
    { label: 'Confirmed', id: 1 },
    { label: 'Preparing', id: 2 },
    { label: 'Served', id: 4 },
    { label: 'Paid', id: 5 }
  ]

  baseUrl = 'https://restaurantapi.bssoln.com/images/'

  constructor(
    private restaurentService: RestaurantService, 
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.getOrderList();

  }

  goToPage(action: any): void {
    switch (action) {
      case 'first':
        this.currentPage = 1;
        break;
      case 'last':
        this.currentPage = this.totalPages;
        break;
      default:
        this.currentPage = action;
        break;
    }
    this.getOrderList();
  }


  getPagesArray(totalPages: number): number[] {
    return Array.from({ length: totalPages }, (_, i) => i + 1);
  }

  
  deleteOrderFirst(){
    this.deleteOrder(this.orderId);
  }


  ToggleFoodStatus(value: string) {
    this.toogleFoodStatus = true;
    this.toggleValue = value;
  }


  openDeleteConfirmationModal(orderId: string): void {
    this.orderId = orderId;
    this.showDeleteConfirmation = true;
  }


  cancelDelete(): void {
    this.showDeleteConfirmation = false;
  }


  getOrderList(): void {
    this.isLoading = true;
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };

    this.restaurentService.getOrderList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.orderList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
            console.log(response);
          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }

  getSortedOrderList(sortingId:number): void {
    console.log(sortingId);
    this.isLoading = true;
    const paginationModel: SortPaginationModel = {
     id:sortingId
    };
    if(sortingId===100){
      this.currentPage=1;
      this.getOrderList();
    }
    else{
      this.restaurentService.getSortedOrderList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.orderList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
          }, 500);
        },
        error: (error) => {
          this.toastr.error(error);
        },
      });
    }

   
  }


  updateStatus(orderId:string,status:number){
      const request: UpdateStatusRequest = {
        status: status
      };
    
      this.restaurentService.updateOrderStatus(orderId, request)
        .subscribe({
          next: (response) => {
            this.toogleFoodStatus = false;
            this.getOrderList();
            this.toastr.success('Status Updated!');

          },
          error: (error) => {
            this.toastr.error('Error updating status.');
          },
        });

  }


  deleteOrder(orderId: string ): void {
    this.restaurentService.deleteOrder(orderId)
    .subscribe({
      next: (response) => {
        this.toastr.success('Deleted Successfully !');
        this.getOrderList(); 
        this.showDeleteConfirmation = false;
      },
      error: (error) => {
        this.toastr.error('Something is Wrong !');
      },
    });
  }


  getDefaultOrderList(): OrderListModel {
    return {
      pageNumber: 1,
      current_page: 1,
      per_page: 10,
      pageSize: 10,
      firstPage: '',
      lastPage: '',
      last_page: 1,
      totalPages: 1,
      totalRecords: 0,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: '',
      prev_page_url: null,
      data: []
    };
  }



}