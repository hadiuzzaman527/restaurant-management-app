import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription, debounceTime, distinctUntilChanged } from 'rxjs';
import { PaginationModel } from 'src/app/shared/models/request/pagination.model';
import { SearchPaginationModel } from 'src/app/shared/models/request/searchPagination.model';
import { FoodItem, FoodListResponse } from 'src/app/shared/models/response/food.model';
import { TableListResponse } from 'src/app/shared/models/response/table.model';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { SharedDataService } from 'src/app/shared/services/shared.service';


interface FoodFormModel {
  search: string;
}


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  tableList: TableListResponse = this.getDefaultTableList();
  foodList: FoodListResponse = this.getDefaultFoodList();
  foodItem: FoodItem[]=[];

  baseUrl = 'https://restaurantapi.bssoln.com/images/'

  parentLoading:boolean=false;
  isLoading: boolean = false;

  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPageOptions: number[] = [10, 20, 30, 50];
  isMenuOpen = false;
  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }
  itemsPerPage: any = 10;
  showDeleteConfirmation = false;
  tableId!: number;
  employeeId!: string;

  foodForm!: FormGroup;
  textSubscription!: Subscription;
  searchText: any;
  sidebar:boolean=false;

  selectedTableId:any;
  selectedFoodList:any[]=[];


  private selectedFoodListSubscription: Subscription;

  constructor(
    private restaurentService: RestaurantService,
    private sharedDataService:SharedDataService,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private _fb: FormBuilder
  ) {
    this.createForm();
    this.getTableList('first');
    this.getFoodList();
    this.selectedFoodListSubscription = this.sharedDataService.selectedFoodList$.subscribe(
      (foodList: any[]) => {
        this.selectedFoodList = foodList;
      }
    );
  }

  ngOnInit(): void {
    this.searchControl?.valueChanges.pipe(debounceTime(500), distinctUntilChanged()).subscribe((value) => {
      this.loadSearchedData(value);
    });
  }

  get searchControl(): AbstractControl | null {
    return this.foodForm.get('search');
  }

  selectedTable(id:any){
    this.selectedTableId=id;
    this.selectedFoodList=[];
    this.sharedDataService.setSelectedTableId(id);
  }

  selectedFood(food: any) {
    const foodWithQuantity = { ...food, quantity: 1 };
    this.selectedFoodList.push(foodWithQuantity);
    
    this.sharedDataService.setSelectedFoodList(this.selectedFoodList);
  }
  

  isFoodItemSelected(food: any): boolean {
    return this.selectedFoodList.some((selectedFood) => selectedFood.id === food.id);
  } 


  loadSearchedData(value:any) {
    this.isLoading = true;
    const paginationModel: SearchPaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
      query: value
    };

    this.restaurentService.getSearchedFoodList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.foodList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
            console.log("Load Data: ",response);

          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }


  getTableList(action: string) {
    this.isLoading = true;
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };

    this.restaurentService.getTableList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.tableList = response;
            this.isLoading = false;
            this.totalPages = response.totalPages;
            if (action === 'first' || action === 'last') {
              this.currentPage = response.current_page;
            }
          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }

  onScrollingFinished() {
    console.log('load more');
    this.currentPage++;
    console.log(this.currentPage <= this.totalPages);
    
    if (this.currentPage <= this.totalPages) {
      this.getFoodList();
    }

  }



  getFoodList() {
    if(this.currentPage===1){
      this.parentLoading=true;
    }
    if(this.currentPage>1){
      this.isLoading = true;
    }
   
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };

    this.restaurentService.getFoodList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.foodItem = [...this.foodItem, ...response.data];
            this.parentLoading=false;
            this.isLoading = false;

            this.totalPages = response.totalPages;

          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }


  getDefaultTableList(): TableListResponse {
    return {
      pageNumber: 0,
      current_page: 0,
      per_page: 0,
      pageSize: 0,
      firstPage: '...',
      lastPage: '...',
      last_page: 0,
      totalPages: 0,
      totalRecords: 0,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: null,
      prev_page_url: null,
      data: [],
    };

  }

  getDefaultFoodList(): FoodListResponse {
    return {
      pageNumber: 1,
      current_page: 1,
      per_page: 0,
      pageSize: 0,
      firstPage: '',
      lastPage: '',
      last_page: 1,
      totalPages: 1,
      totalRecords: 4,
      total: 0,
      from: 0,
      to: 0,
      next_page_url: null,
      prev_page_url: null,
      data: []
    };
  }

  private createForm(): void {
    this.foodForm = this._fb.group({
      search: ['', [Validators.required]],
    } as unknown as FoodFormModel);
  }




}






