export interface MenuItem {
    label: string;
    routerLink: string;
    icon: any; 
    isActive?: boolean; 
  }
  