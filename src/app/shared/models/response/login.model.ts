export interface LoginResponse {
    token: string;
    user: User;
  }
  
  export interface User {
    id: string;
    fullName: string;
    email: string;
    image: string | null;
    userName: string;
    phoneNumber: string;
  }
