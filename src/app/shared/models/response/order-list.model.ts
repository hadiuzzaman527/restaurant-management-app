export interface OrderListModel {
    pageNumber: number;
    current_page: number;
    per_page: number;
    pageSize: number;
    firstPage: string;
    lastPage: string;
    last_page: number;
    totalPages: number;
    totalRecords: number;
    total: number;
    from: number;
    to: number;
    next_page_url: string;
    prev_page_url: string | null;
    data: OrderItem[];
  }
  
  export interface OrderItem {
    id: string;
    orderNumber: string;
    amount: number;
    orderStatus: string;
    orderTime: string;
    table: TableItem;
    orderedBy: any; 
    orderTakenBy: any; 
    orderItems: OrderFoodItem[];
  }
  
  export interface TableItem {
    id: number;
    tableNumber: string;
    numberOfSeats: number;
    isOccupied: boolean;
    image: string;
    employees: any; 
  }
  
  export interface OrderFoodItem {
    id: string;
    quantity: number;
    unitPrice: number;
    totalPrice: number;
    food: FoodItem;
    foodPackage: any; 
  }
  
  export interface FoodItem {
    id: number;
    name: string;
    description: string | null;
    price: number;
    discountType: string | null;
    discount: number;
    discountPrice: number;
    image: string;
  }
  