
export interface Employee {
    employeeTableId: number;
    employeeId: string;
    name: string;
}


export interface Table {
    id: number;
    tableNumber: string;
    numberOfSeats: number;
    isOccupied: boolean;
    image: string;
    employees: Employee[];
}


export interface TableListResponse {
    pageNumber: number;
    current_page: number;
    per_page: number;
    pageSize: number;
    firstPage: string;
    lastPage: string;
    last_page: number;
    totalPages: number;
    totalRecords: number;
    total: number;
    from: number;
    to: number;
    next_page_url: string | null;
    prev_page_url: string | null;
    data: Table[];
}
