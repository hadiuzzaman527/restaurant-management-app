
export interface Food {
    foodId: number;
    foodPackageId: any;
    quantity: number;
    unitPrice: number;
    totalPrice: number;
}

export interface OrderRequest {
    tableId: number;
    orderNumber: string;
    amount: number;
    phoneNumber: any;
    items: Food[];
}
