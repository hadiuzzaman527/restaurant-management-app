export interface SearchPaginationModel {
    page: number;
    size: number;
    query:any;
}