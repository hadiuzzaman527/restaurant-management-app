export interface AddEmployeeModel {
    designation: string;
    joinDate: Date;
    email: string;
    phoneNumber: string;
    firstName: string;
    middleName: string;
    lastName: string;
    fatherName: string;
    motherName: string;
    spouseName: string;
    dob: Date;
    nid: string;
    genderId: number;
    image: string;
    base64: string;
  }
  