export interface AddTableRequest {
    tableNumber: string;
    numberOfSeats: number;
    image: string;
    base64: string;
  }
  