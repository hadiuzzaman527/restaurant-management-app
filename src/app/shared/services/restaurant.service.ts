import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { EmployeeListResponse } from "../models/response/employee.model";
import { PaginationModel } from "../models/request/pagination.model";
import { TableListResponse } from "../models/response/table.model";
import { FoodListResponse } from "../models/response/food.model";
import { SearchPaginationModel } from "../models/request/searchPagination.model";
import { NonAssignEmployee } from "../models/request/non-assign-employee.model";
import { AssignEmployeeModel } from "../models/request/assign-employee.model";
import { OrderListModel } from "../models/response/order-list.model";
import {UpdateStatusRequest} from "../models/request/update-status.model";
import { SortPaginationModel } from "../models/request/sortPagination.model";
import { OrderRequest } from "../models/request/order.model";
import { AddEmployeeModel } from "../models/request/add-employee.model";
import { FoodRequest } from "../models/request/add-food.model";
import { AddTableRequest } from "../models/request/add-table.model";

@Injectable()
export class RestaurantService {

  private baseUrl = 'https://restaurantapi.bssoln.com/api';

  constructor(private httpClient: HttpClient) { }
  
  getEmployeeList(request: PaginationModel): Observable<EmployeeListResponse> {
    const params = new HttpParams()
      .set('Page', request.page.toString())
      .set('Per_Page', request.size.toString());

    return this.httpClient.get<EmployeeListResponse>(
      `${this.baseUrl}/Employee/datatable`,
      { params }
    );
  }


  addEmployee(request: AddEmployeeModel): Observable<any> {
    const url = `${this.baseUrl}/Employee/create`;
    return this.httpClient.post<any>(url, request);
  }

  deleteEmployee(id: string) {
    const url = `${this.baseUrl}/Employee/delete/${id}`;
    return this.httpClient.delete<any>(url);
  }


  getTableList(request: PaginationModel): Observable<TableListResponse> {
    const params = new HttpParams()
      .set('Page', request.page.toString())
      .set('Per_Page', request.size.toString());

    return this.httpClient.get<TableListResponse>(
      `${this.baseUrl}/Table/datatable`,
      { params }
    );
  }


  addTable(request:AddTableRequest): Observable<any> {
    const url = `${this.baseUrl}/Table/create`;
    return this.httpClient.post<any>(url, request);
  }


  deleteTable(id: number) {
    const url = `${this.baseUrl}/Table/delete/${id}`;
    return this.httpClient.delete<any>(url);
  }


  getFoodList(request: PaginationModel): Observable<FoodListResponse> {
    const params = new HttpParams()
      .set('Page', request.page.toString())
      .set('Per_Page', request.size.toString());

    return this.httpClient.get<FoodListResponse>(
      `${this.baseUrl}/Food/datatable`,
      { params }
    );
  }

  getSearchedFoodList(request: SearchPaginationModel): Observable<FoodListResponse> {
    const params = new HttpParams()
      .set('Search', request.query.toString());

    return this.httpClient.get<FoodListResponse>(
      `${this.baseUrl}/Food/datatable`,
      { params }
    );
  }

  addFood(request:FoodRequest): Observable<any> {
    const url = `${this.baseUrl}/Food/create`;
    return this.httpClient.post<any>(url, request);
  }


  deleteFood(id: number) {
    const url = `${this.baseUrl}/Food/delete/${id}`;
    return this.httpClient.delete<any>(url);
  }


  getNonAssignEmployee(id: number): Observable<NonAssignEmployee[]> {
    const url = `${this.baseUrl}/Employee/non-assigned-employees/${id}`;
    return this.httpClient.get<NonAssignEmployee[]>(url);
  }

  addNonAssignEmployee(request: AssignEmployeeModel[]): Observable<AssignEmployeeModel[]> {
    const url = `${this.baseUrl}/EmployeeTable/create-range`;
    return this.httpClient.post<AssignEmployeeModel[]>(url, request);
  }


  getOrderList(request: PaginationModel): Observable<OrderListModel> {
    const params = new HttpParams()
      .set('Page', request.page.toString())
      .set('Per_Page', request.size.toString());

    return this.httpClient.get<OrderListModel>(
      `${this.baseUrl}/Order/datatable`,
      { params }
    );
  }


  updateOrderStatus(orderId: string, request: UpdateStatusRequest): Observable<any> {
    const url = `${this.baseUrl}/Order/update-status/${orderId}`;
    return this.httpClient.put(url, request);
  }


  getSortedOrderList(request: SortPaginationModel): Observable<OrderListModel> {
    const params = new HttpParams()
      .set('Search', request.id);

    return this.httpClient.get<OrderListModel>(
      `${this.baseUrl}/Order/datatable`,
      { params }
    );
  }

  addOrder(request:OrderRequest):Observable<any> {
    const url = `${this.baseUrl}/Order/create`;
    return this.httpClient.post<any>(url, request);
  }

  deleteOrder(id: string) {
    const url = `${this.baseUrl}/Order/delete/${id}`;
    return this.httpClient.delete<any>(url);
  }


}




