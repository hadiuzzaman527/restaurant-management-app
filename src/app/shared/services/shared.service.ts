import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FoodListResponse } from '../models/response/food.model';

@Injectable({
    providedIn: 'root'
})
export class SharedDataService {

    constructor(private zone: NgZone) {     
        this.categoriesSubject.next(this.categories);
      
    }

    private refreshTableListSubject = new BehaviorSubject<undefined>(undefined);
    refreshTableList$ = this.refreshTableListSubject.asObservable();

    private selectedTableIdSubject = new BehaviorSubject<number | null>(null);
    selectedTableId$ = this.selectedTableIdSubject.asObservable();

    private selectedFoodListSubject = new BehaviorSubject<any[]>([]);
    selectedFoodList$ = this.selectedFoodListSubject.asObservable();

    private FoodListSubject = new BehaviorSubject<FoodListResponse[]>([]);
    FoodList$ = this.FoodListSubject.asObservable();

    private currentPageSubject = new BehaviorSubject<number>(1);
    currentPage$ = this.currentPageSubject.asObservable();

    refreshTableList() {
        this.refreshTableListSubject.next(undefined);
    }

    setSelectedTableId(tableId: number): void {
        this.zone.run(() => {
            this.selectedFoodListSubject.next([]);
            this.selectedTableIdSubject.next(tableId);
        });
    }

    setSelectedFoodList(foodList: any[]): void {
        this.zone.run(() => {
            this.selectedFoodListSubject.next(foodList);
        });
    }

    setSelectedFoodListWithPage(data: FoodListResponse): void {
        this.zone.run(() => {
            const currentFoodList = this.FoodListSubject.getValue();
            const existingDataIndex = currentFoodList.findIndex(item => item.pageNumber === data.current_page);
    
            if (existingDataIndex !== -1) {
               
                currentFoodList[existingDataIndex] = data;
                console.log(this.FoodList$);
            } else {
              
                const updatedFoodList: FoodListResponse[] = [...currentFoodList, data];
                this.FoodListSubject.next(updatedFoodList);
                console.log(this.FoodList$);
            }
    
            this.currentPageSubject.next(data.current_page);
        });
    }
    

    getSelectedFoodListForPage(page: number): FoodListResponse | null {
        const foodListByPage = this.FoodListSubject.getValue();
    
        const existingData = foodListByPage.find(item => item.pageNumber === page);
    
        if (existingData) {
            return existingData;
        } else {
            return null;
        }
    }
    



    private categoriesSubject = new BehaviorSubject<Array<string>>([]);
    categories$ = this.categoriesSubject.asObservable();
  
    categories: Array<string> = [];
    allCategories: Array<string> = Array.from({ length: 200 }, (_, i) => `Item - ${i}`);
  

    loadMore(): void {
      if (this.getNextItems()) {
        this.categoriesSubject.next(this.categories);
      }
    }
  
    getNextItems(): boolean {
      if (this.categories.length >= this.allCategories.length) {
        return false;
      }
      const remainingLength = Math.min(50, this.allCategories.length - this.categories.length);
      this.categories.push(...this.allCategories.slice(this.categories.length, this.categories.length + remainingLength));
      return true;
    }



}