import { Observable, of } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { tap } from 'rxjs/operators';
import { LoginRequest } from "../models/request/login.model";
import { LoginResponse } from "../models/response/login.model";


@Injectable()
export class AuthenticationService {

  private _baseUrl = "https://restaurantapi.bssoln.com/api";

  constructor(private _httpClient: HttpClient) { }

  login(request: LoginRequest): Observable<LoginResponse> {
    return this._httpClient.post<LoginResponse>(`${this._baseUrl}/Auth/SignIn`, request)
      .pipe(
        tap((loginResponse: LoginResponse) => {
          this.saveTokensToLocalStorage(loginResponse.token);
        })
      );
  }

  saveTokensToLocalStorage(token: string) {
    localStorage.setItem('token',token);
  }

}
