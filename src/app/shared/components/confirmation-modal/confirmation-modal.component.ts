import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html'
})
export class ConfirmationModalComponent {
  @Input() modalContent:string=''

  @Output() confirmed = new EventEmitter<void>();
  @Output() canceled = new EventEmitter<void>();


  onConfirmClick(): void {
    this.confirmed.emit();
  }

  onCancelClick(): void {
    this.canceled.emit();
  }
}
