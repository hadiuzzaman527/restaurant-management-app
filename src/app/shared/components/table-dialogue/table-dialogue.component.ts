import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AssignEmployeeModel } from '../../models/request/assign-employee.model';
import { RestaurantService } from '../../services/restaurant.service';
import { NonAssignEmployee } from '../../models/request/non-assign-employee.model';
import { SharedDataService } from '../../services/shared.service';

@Component({
  selector: 'app-table-dialogue',
  templateUrl: './table-dialogue.component.html',
  styleUrls: ['./table-dialogue.component.css']
})
export class TableDialogueComponent implements OnInit{
  toppings = new FormControl('');

  selectedEmployees: any[] = [];
  tableId:number=0;;
  numberOfSeats:number=0;


  NonAssignEmployee: NonAssignEmployee[]=[];
  assignEmployee: AssignEmployeeModel[]=[]

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sharedService: SharedDataService,
    private restaurentService:RestaurantService,
    public dialogRef: MatDialogRef< TableDialogueComponent>


  )
    {

      this.tableId = data.tableId;
      this.numberOfSeats = data.numberOfSeats;

     }
     ngOnInit(): void {
      console.log(this.tableId)
      if(this.tableId){
        this.nonAssignEmployee(this.tableId);
      }

    }

  closeDialog(): void {
    this.dialogRef.close();

  }

  nonAssignEmployee(tableId:number){
    this.restaurentService.getNonAssignEmployee(tableId)
    .subscribe({
      next: (response) => {
        setTimeout(() => {
         this.NonAssignEmployee=response;
          console.log(this.NonAssignEmployee);

        }, 500); 
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  addAssignEmployee(){
    console.log(this.selectedEmployees)
    const requests: AssignEmployeeModel[] = this.selectedEmployees.map(employee => {
      return {
        employeeId: employee.employeeId,
        tableId: this.tableId
      };
    });
      console.log(requests)
      this.restaurentService.addNonAssignEmployee(requests)
      .subscribe({
        next: (response) => {
          this.sharedService.refreshTableList(); 
          this.dialogRef.close();
        },
        error: (error) => {
          console.error(error);
        },
      });
  }
}


