import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Output } from '@angular/core';
import { faBagShopping, faShoppingBag} from '@fortawesome/free-solid-svg-icons';
import { SharedDataService } from '../../services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent {
  faShoppingBag = faShoppingBag;
  isMenuOpen = false;
  isCartOpen=false;

  selectedFoodListLength:number=0;

  @Output() openNavValue: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private sharedDataService: SharedDataService,
    private _cdRef: ChangeDetectorRef,
  ) {
    this.sharedDataService.selectedFoodList$.subscribe((foodList) => {
      this.selectedFoodListLength = foodList.length;
      this._cdRef.detectChanges();
    });
  }

  navToggle() {
    this.isMenuOpen = !this.isMenuOpen;
    this.openNavValue.emit(this.isMenuOpen);
  }

  cartToggle(){
    this.isCartOpen = !this.isCartOpen;
    this._cdRef.detectChanges();
  }

  handleCloseCart(){
    console.log(this.isCartOpen);
    this.isCartOpen = !this.isCartOpen;
    this._cdRef.detectChanges();
  }


  handleCloseSidebar(){
    console.log(this.isCartOpen);
    this.isMenuOpen = !this.isMenuOpen;
    this._cdRef.detectChanges();
  }

}
