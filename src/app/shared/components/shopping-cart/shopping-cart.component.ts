import { Component, ChangeDetectorRef, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { SharedDataService } from '../../services/shared.service';
import { OrderRequest } from '../../models/request/order.model';
import { RestaurantService } from '../../services/restaurant.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css'],
})
export class ShoppingCartComponent implements OnInit {
  @Input() isCartOpen: boolean = false;

  @Output() closeCartValue: EventEmitter<void> = new EventEmitter<void>;

  faShoppingBag = faShoppingBag;
  isOpen = false;

  selectedFoodList: any[] = [];
  selectedTableId:any;
 
  baseUrl = 'https://restaurantapi.bssoln.com/images/';

  constructor(
    private restaurentService:RestaurantService,
    private sharedDataService: SharedDataService,
    private toastr: ToastrService,
    private _router: Router,
    private _cdRef: ChangeDetectorRef,
  ) {}
  
  ngOnInit() {
    this.sharedDataService.selectedFoodList$.subscribe((foodList) => {
      this.selectedFoodList = foodList;
      this._cdRef.detectChanges();
    });

    this.sharedDataService.selectedTableId$.subscribe((tableId) => {
      this.selectedTableId = tableId;
      this._cdRef.detectChanges();
    });
  }

  closeCart() {
    this.sharedDataService.setSelectedFoodList(this.selectedFoodList);
    this.isCartOpen = false;
    this.closeCartValue.emit();
   
    this._cdRef.detectChanges();
  }

  increaseQuantity(item: any) {
    item.quantity += 1;
    this._cdRef.detectChanges();
  }

  decreaseQuantity(item: any) {
    if (item.quantity > 1) {
      item.quantity -= 1;
      this._cdRef.detectChanges();
    }
  }

  calculateTotalPrice(item: any): number {
    return item.price * item.quantity;
  }

  getTotalPrice(): number {
    return this.selectedFoodList.reduce((total, item) => total + this.calculateTotalPrice(item), 0);
  }

  removeItem(item:any){
    this.selectedFoodList = this.selectedFoodList.filter((selectedItem) => selectedItem !== item);
    this.sharedDataService.setSelectedFoodList(this.selectedFoodList);
  }

  confirmOrder() {
    const currentDateTime = new Date().getTime(); 
    const orderNumber = currentDateTime.toString(); 

    const request:OrderRequest = {
      tableId: this.selectedTableId,
      orderNumber: orderNumber, 
      amount: this.getTotalPrice(),
      phoneNumber: null, 
      items: this.selectedFoodList.map(item => ({
        foodId: item.id,
        foodPackageId: null,
        quantity: item.quantity,
        unitPrice: item.price,
        totalPrice: this.calculateTotalPrice(item),
      })),
    };

    this.restaurentService.addOrder(request)
    .subscribe({
      next: (response) => {
        this.toastr.success('Order is Confirmed !');
        this.sharedDataService.setSelectedFoodList([]);
        this._router.navigate([`dashboard/order-list`], { replaceUrl: true});
      },
      error: (error) => {
        this.toastr.error('Something is Wrong !');
        this._router.navigate([`dashboard/order-list`], { replaceUrl: true});
      },
    });



   
}



}

