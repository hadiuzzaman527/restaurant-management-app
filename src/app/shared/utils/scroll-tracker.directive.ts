// import { Directive, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

// @Directive({
//     selector: '[scrollTracker]'
// })
// export class ScrollTrackerDirective {
//     @Output() scrollingFinished = new EventEmitter<void>();

//     emitted = false;

//     constructor(private el: ElementRef) { }

//     @HostListener("scroll", [])
//     onScroll(): void {
//         const element = this.el.nativeElement;
//         console.log("element.scrollHeight: ", element.scrollHeight);
//         console.log("element.scrollTop: ", element.scrollTop);
//         console.log("element.scrollHeight - element.scrollTop: ", element.scrollHeight - element.scrollTop);

//         console.log("element.clientHeight: ", element.clientHeight);

//         const difference = Math.abs(element.scrollHeight - element.scrollTop - element.clientHeight);
//         console.log("difference: ", difference);
//         const threshold = 1;

//         if (difference <= threshold && !this.emitted) {
//             this.scrollingFinished.emit();
//         } else if ((element.scrollHeight - element.scrollTop) < element.clientHeight) {
//             this.emitted = false;
//         }
//     }

// }


import { Directive, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[scrollTracker]'
})
export class ScrollTrackerDirective {
  @Output() scrollingFinished = new EventEmitter<void>();
  private emitted = false;
  private scrollSubject = new Subject<Event>();

  constructor(private el: ElementRef) {
    this.scrollSubject.pipe(debounceTime(200)).subscribe(() => {
      this.checkScroll();
    });
  }

  @HostListener("scroll", ['$event'])
  onScroll(event: Event): void {
    this.scrollSubject.next(event);
  }

  private checkScroll(): void {
    const element = this.el.nativeElement;
            console.log("element.scrollHeight: ", element.scrollHeight);
        console.log("element.scrollTop: ", element.scrollTop);
        console.log("element.scrollHeight - element.scrollTop: ", element.scrollHeight - element.scrollTop);
    const difference = Math.abs(element.scrollHeight - element.scrollTop - element.clientHeight);
    const threshold = 1;

    if (difference <= threshold && !this.emitted) {
      this.emitted = true;
      this.scrollingFinished.emit();
    } else if ((element.scrollHeight - element.scrollTop) < element.clientHeight) {
      this.emitted = false;
    }
  }
}

