import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ScrollPaginationService } from '../scroll-pagination.service';
import { FoodItem, FoodListResponse } from '../../models/response/food.model';
import { RestaurantService } from '../../services/restaurant.service';
import { SharedDataService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { PaginationModel } from '../../models/request/pagination.model';

@Component({
  selector: 'app-scroll-pagination',
  templateUrl: './scroll-pagination.component.html',
  styleUrls: ['./scroll-pagination.component.css']
})
export class ScrollPaginationComponent {
  foodItem: FoodItem[]=[];
  parentLoader:boolean=false;
  isLoading: boolean = false;
  currentPage: any = 1;
  totalPages: any = 1;
  itemsPerPage: any = 5;
  baseUrl = 'https://restaurantapi.bssoln.com/images/'
  sidebar:boolean=false;
  constructor(
    private restaurentService: RestaurantService,
    private sharedDataService: SharedDataService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getFoodList();
    this.getFoodList();
  }


  onScrollingFinished() {
    console.log('load more');
    this.currentPage++;
    console.log(this.currentPage <= this.totalPages);
    
    if (this.currentPage <= this.totalPages) {
      this.getFoodList();
    }

  }


  getFoodList() {
    if(this.currentPage===1){
      this.parentLoader=true;
    }
    if(this.currentPage>1){
      this.isLoading = true;
    }
   
    const paginationModel: PaginationModel = {
      page: this.currentPage,
      size: this.itemsPerPage,
    };

    this.restaurentService.getFoodList(paginationModel)
      .subscribe({
        next: (response) => {
          setTimeout(() => {
            this.foodItem = [...this.foodItem, ...response.data];
            this.parentLoader=false;
            this.isLoading = false;

            this.totalPages = response.totalPages;

          }, 500);
        },
        error: (error) => {
          console.error(error);
        },
      });
  }


getDefaultFoodList(): FoodListResponse {
  return {
    pageNumber: 1,
    current_page: 1,
    per_page: 0,
    pageSize: 0,
    firstPage: '',
    lastPage: '',
    last_page: 1,
    totalPages: 1,
    totalRecords: 4,
    total: 0,
    from: 0,
    to: 0,
    next_page_url: null,
    prev_page_url: null,
    data: []
  };
}




}
