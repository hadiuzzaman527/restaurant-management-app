import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './features/auth/login/login.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './shared/components/app-header/app-header.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { HomeComponent } from './features/dashboard/home/home.component';
import { EmployeeListComponent } from './features/dashboard/employee/employee-list/employee-list.component';
import { TableListComponent } from './features/dashboard/table/table-list/table-list.component';
import {MatInputModule} from '@angular/material/input';
import { AuthenticationService } from './shared/services/auth.service';
import { RestaurantService } from './shared/services/restaurant.service';
import { TokenInterceptor } from './core/interceptors/token.interceptor';
import { MatIconModule } from '@angular/material/icon';
import { ConfirmationModalComponent } from './shared/components/confirmation-modal/confirmation-modal.component';
import { AddEmployeeComponent } from './features/dashboard/employee/add-employee/add-employee.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FoodListComponent } from './features/dashboard/food/food-list/food-list.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { TableDialogueComponent } from './shared/components/table-dialogue/table-dialogue.component';
import {FormControl} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OrderComponent } from './features/dashboard/order/order.component';
import { OrderListComponent } from './features/dashboard/order/order-list/order-list.component';
import { ShoppingCartComponent } from './shared/components/shopping-cart/shopping-cart.component';
import { DatePipe } from '@angular/common';
import { AddFoodComponent } from './features/dashboard/food/add-food/add-food.component';
import { AddTableComponent } from './features/dashboard/table/add-table/add-table.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ScrollTrackerDirective } from './shared/utils/scroll-tracker.directive';
import { ScrollPaginationComponent } from './shared/utils/scroll-pagination/scroll-pagination.component';
import { ScrollPaginationService } from './shared/utils/scroll-pagination.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    EmployeeListComponent,
    TableListComponent,
    ConfirmationModalComponent,
    AddEmployeeComponent,
    FoodListComponent,
    TableDialogueComponent,
    OrderComponent,
    OrderListComponent,
    ShoppingCartComponent,
    AddFoodComponent,
    AddTableComponent,
    ScrollTrackerDirective,
    ScrollPaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    FontAwesomeModule,


  ],
  providers: [AuthenticationService,RestaurantService,ScrollPaginationService, DatePipe,{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
  },],
  bootstrap: [AppComponent]
})
export class AppModule { }
