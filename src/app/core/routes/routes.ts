import { Routes } from "@angular/router";
import { LoginComponent } from "src/app/features/auth/login/login.component";
import { DashboardComponent } from "src/app/features/dashboard/dashboard.component";
import { AddEmployeeComponent } from "src/app/features/dashboard/employee/add-employee/add-employee.component";
import { EmployeeListComponent } from "src/app/features/dashboard/employee/employee-list/employee-list.component";
import { AddFoodComponent } from "src/app/features/dashboard/food/add-food/add-food.component";
import { FoodListComponent } from "src/app/features/dashboard/food/food-list/food-list.component";
import { HomeComponent } from "src/app/features/dashboard/home/home.component";
import { OrderListComponent } from "src/app/features/dashboard/order/order-list/order-list.component";
import { OrderComponent } from "src/app/features/dashboard/order/order.component";
import { AddTableComponent } from "src/app/features/dashboard/table/add-table/add-table.component";
import { TableListComponent } from "src/app/features/dashboard/table/table-list/table-list.component";
import { ScrollPaginationComponent } from "src/app/shared/utils/scroll-pagination/scroll-pagination.component";

export const routes: Routes = [
    {path:'',redirectTo: '/login', pathMatch: 'full'},
    {path:'login', component:LoginComponent},
 
    {path:'dashboard', component:DashboardComponent, children:[
      {path:'home', component:HomeComponent},
      {path:'employee-list', component:EmployeeListComponent},
      {path:'add-employee', component:AddEmployeeComponent},
      {path:'table-list', component:TableListComponent},
      {path:'food-list', component:FoodListComponent},
      {path:'order', component:OrderComponent},
      {path:'order-list', component:OrderListComponent},
      {path:'add-food', component:AddFoodComponent},
      {path:'add-table', component:AddTableComponent},
      {path:'scroll-pagination', component:ScrollPaginationComponent},
    ]},
  ];

